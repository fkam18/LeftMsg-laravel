<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/msg', 'MsgController@getmsg');
Route::get('/add', 'MsgController@create');
Route::post('/save', 'MsgController@save');
Route::get('/', 'MsgController@create');
